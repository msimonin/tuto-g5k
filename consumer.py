#!/usr/bin/env python
import pika
import time

import sys

host = sys.argv[1]

total = 0

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=host))
channel = connection.channel()

channel.queue_declare(queue='task_queue')
print(' [*] Waiting for messages. To exit press CTRL+C')


def callback(ch, method, properties, body):
    global total
    print(" [x] Received msg %r" % body.decode())
    time.sleep(body.count(b'.'))
    print(f" [x] Done with message {total}")
    # increment the total number of message received
    total += 1
    # ack the message back to the broker
    ch.basic_ack(delivery_tag=method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='task_queue', on_message_callback=callback)

channel.start_consuming()