from enoslib.api import discover_networks, play_on, run_command
from enoslib.infra.enos_g5k.provider import G5k
from enoslib.infra.enos_g5k.configuration import Configuration, NetworkConfiguration
from enoslib.service import SimpleNetem

import logging


logging.basicConfig(level=logging.DEBUG)

TMUX_SESSION = "iperf"


#
# Reservation section
# - two nodes and a network in between
#
network = NetworkConfiguration(
    id="n1", type="prod", roles=["mynetwork"], site="nantes"
)

conf = (
    Configuration.from_settings(job_name="iperf", job_type="allow_classic_ssh")
    .add_network_conf(network)
    .add_machine(
        roles=["server"], cluster="econome", nodes=1, primary_network=network
    )
    .add_machine(
        roles=["client"], cluster="econome", nodes=1, primary_network=network
    )
    .finalize()
)

provider = G5k(conf)
roles, networks = provider.init()
roles = discover_networks(roles, networks)


#
# Install and launch iperf
#
with play_on(roles=roles) as p:
    # flent requires python3, so we default python to python3
    p.apt(name=["iperf", "tmux"], state="present", update_cache=True)

with play_on(pattern_hosts="server", roles=roles) as p:
    # Start the iperf server
    # if you want to check the server outputs
    # $) tmux a
    p.shell(
            (
                f"(tmux ls | grep {TMUX_SESSION}) || "
                f"tmux new-session -s {TMUX_SESSION} "
                f"-d 'exec iperf -s'"
            ),
            display_name=f"Running iperf in server mode")


# Start a transfer here and grab the outputs
result = run_command(
    f"iperf -c {roles['server'][0].address}", roles=roles, pattern_hosts="client"
)
print(result)
