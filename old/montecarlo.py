#!/usr/bin/env python3
"""
Program that approximates PI using a montecarlo approach.
It's a naive program to illustrate massively parallel computing with a
production testbed.

Principles:

We randomly draw point in the square [0,1] x [0,1] and count those inside the
circle x^2 + y^2=1.

The ratio #point_inside_circle/#total is an approximation of pi/4 (the
surface of the quarter of the circle).

This method doesn't converge rapidly so we need a huge amount of point.

We print the number of point inside and the total of point in stdout, and
some progress.
"""

from random import random

N = int(1e7)
inside = 0
for i in range(N):
    x, y = random(), random()
    if x*x + y*y <= 1:
        inside += 1
    if i % int(N/10) == 0:
        print(f"progress = {100 * i/N} %")

print("\n---- pi estimation ----")
print(f"{inside} {N} {4*inside/N}")