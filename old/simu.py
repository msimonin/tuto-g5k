#!/usr/bin/env python3

import argparse
import json
import logging
from multiprocessing import Pool
from pathlib import Path
import tempfile

from simgrid import Engine, Mailbox, this_actor

LOGGER = logging.getLogger(__name__)

TEMPLATE_PLATFORM = Path("xmls/platform.xml")
TEMPLATE_DEPLOYMENT = Path("xmls/deployment.xml")

# some globals
latency = None
bandwidth = None
size = None


class Sender:
    def __init__(self, *args):
        if len(args) != 1:
            raise AssertionError(
                "Actor sender requires 1 parameter, but got {:d}".format(len(args))
            )
        self.messages_count = 1
        self.msg_size = int(args[0])  # communication cost (in bytes)
        self.receivers_count = 1

    def __call__(self):
        # List in which we store all ongoing communications
        pending_comms = []

        # Vector of the used mailboxes
        mboxes = [
            Mailbox.by_name("receiver-{:d}".format(i))
            for i in range(0, self.receivers_count)
        ]

        # Start dispatching all messages to receivers, in a round robin fashion
        for i in range(0, self.messages_count):
            content = "Message {:d}".format(i)
            mbox = mboxes[i % self.receivers_count]

            this_actor.info("Send '{:s}' to '{:s}'".format(content, str(mbox)))

            # Create a communication representing the ongoing communication, and store it in pending_comms
            comm = mbox.put_async(content, self.msg_size)
            pending_comms.append(comm)

        # Start sending messages to let the workers know that they should stop
        for i in range(0, self.receivers_count):
            mbox = mboxes[i]
            this_actor.info("Send 'finalize' to '{:s}'".format(str(mbox)))
            comm = mbox.put_async("finalize", 0)
            pending_comms.append(comm)

        this_actor.info("Done dispatching all messages")

        # Now that all message exchanges were initiated, wait for their completion, in order of creation.
        for comm in pending_comms:
            comm.wait()
        this_actor.info("Goodbye now!")


class Receiver:
    def __init__(self, *args):
        if len(args) != 1:  # Receiver actor expects 1 argument: its ID
            raise AssertionError(
                "Actor receiver requires 1 parameter, but got {:d}".format(len(args))
            )
        self.mbox = Mailbox.by_name("receiver-{:s}".format(args[0]))

    def __call__(self):
        this_actor.info("Wait for my first message")
        while True:
            received = self.mbox.get()
            this_actor.info("I got a '{:s}'.".format(received))
            if received == "finalize":
                break
            else:
                d = dict(
                    latency=latency,
                    bandwidth=bandwidth,
                    size=size,
                    duration=Engine.get_clock(),
                )
                result = Path("result.json")
                with result.open("a") as f:
                    f.write(json.dumps(d))
                    f.write("\n")
                this_actor.info(f"Result appended to {result}")


def simu(generated_platform, generated_deployment):
    # weird we need to pass *something*
    e = Engine([""])

    e.load_platform(str(generated_platform))

    e.register_actor("sender", Sender)
    e.register_actor("receiver", Receiver)

    e.load_deployment(str(generated_deployment))

    e.run()


def simulate(l, b, s):
    global latency, bandwidth, size
    latency = int(l)
    bandwidth = int(b)
    size = int(s)
    with tempfile.TemporaryDirectory() as tempdir:
        generated_platform = Path(tempdir) / "platform.xml"
        generated_deployment = Path(tempdir) / "depoyment.xml"
        with TEMPLATE_PLATFORM.open() as f:
            template_platform = f.read()
            with generated_platform.open("w") as g:
                new_file = template_platform.replace("LATENCY", str(latency)).replace(
                    "BANDWIDTH", str(bandwidth)
                )
                g.write(new_file)
                LOGGER.debug(new_file)

        with TEMPLATE_DEPLOYMENT.open() as f:
            template_deployment = f.read()
            with generated_deployment.open("w") as g:
                new_file = template_deployment.replace("SIZE", str(size))
                g.write(new_file)
                LOGGER.debug(new_file)

        # subprocess it
        with Pool(processes=1) as p:
            p.apply(simu, (generated_platform, generated_deployment))


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    # A loop to get many simulation
    # for l in range(1, 500, 10):
    #    # latency in ms
    #    for b in [10, 100, 1000, 10000]:
    #        # bandwidth in Mbps
    #        for s in [1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e9]:
    #            # size in Byte
    #            simulate(l, b, s)

    # a single simulation
    # latency in ms
    l = 10
    # bandwidth in Mbps
    b = 1000
    # size in byte
    s = 1e6
    simulate(l, b, s)
