#!/usr/bin/env bash

# Load the python bindings shared library
#
# This is useful if simgrid hasn't been 'make installed' but 'pip installed'.
# In the latter the required shared library lies in the python python
# site-packages which isn't loaded by ld by default so we add it to the
# LD_LIBRARY_PATH variable.
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(python <(cat <<HERE
import os
import sys
extended = ":".join([p for p in sys.path if "site-packages" in p])
print(extended)
HERE
))
