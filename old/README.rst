G5K lab
=======

Working with G5K 101
--------------------

Things you need to understand when working with G5K:

- https://www.grid5000.fr/w/Grid5000:UsagePolicy
- SSH connections (https://www.grid5000.fr/w/Getting_Started#Connecting_for_the_first_time)
- Jobs
    - A job encapsulates your application (single process or multi processes)
    - A job is the unit of scheduling
        - job = application + time constraint (walltime) + resource constraint (#core, #ram ...)
        - lifecycle managed by the scheduler (OAR)
        - Job status (simplified)
            - Waiting: the job has been accepted by the scheduler and is
              waiting for an allocation or waiting to start.
            - Running: the job (thus your application is running)
            - Terminated: the job is finished
    - Example
        - ``oarsub -I -l "core=1"``: will start **an interactive** session. This
          session will use one core of a machine (decided by the scheduler)
        - ``oarsub -l "core=1" myprogram``: will start ``myprogram`` using 1
          core. The processing will occur in **batch mode** (in the background,
          start time depend on the scheduler)
    - Some refs:
        - http://oar.imag.fr/docs/2.5/user/usecases.html
        - https://www.grid5000.fr/w/Getting_Started#Discovering.2C_visualizing_and_reserving_Grid.275000_resources
        - man pages:
            - ``man oarsub`` (submit / connect to a job)
            - ``man oarstat`` (display jobs statistics)
            - ``man oardel`` (delete a job)

Get the code
------------

- In the frontend of your choice get the code:
  ``frontend) git clone https://gitlab.inria.fr/msimonin/tuto-g5k``

Warm up: A first example
------------------------

- Start an interactive session using 1 core:
    - ``oarsub -I -l "core=1"``
    - Test the program: ``./montecarlo.py``
    - Exit the job
- Start the job in batch mode
    - ``oarsub -l "core=1" ./montecarlo.py``
    - Check the log files for sterr and stdout (you should find an estimate of PI in stdout)
- Start the job in a low priority setting
    - ``oarsub -l "core=1" -t "besteffort" ./montecarlo.py``
    - ``besteffort`` job can be killed by higher prioriry job
- Start the job in a low priority setting + automatic restart mode
    - ``oarsub -l "core=1" -t "besteffort" -t "idempotent"  ./montecarlo.py``
    - If the job is killed, the scheduler will restart it (beware that you
      can have incomplete outputs though for some jobs)
- Lab Assignement:
    - Get a better estimate for PI using 100 jobs and 5 minutes computation each (approximately).
      In the ideal case where all the jobs run in parallel, you'll have a result in 5 minutes.
    - There's a magic option in ``oarsub`` for that.


Assignement
-----------

Let's consider a situation where two machines are communicating through a
network link. This link has some characteristics (latency, bandwidth) and the
data to transfer has a given size. We're interested in the duration of a data
transfer given the above parameters.


A simulation
~~~~~~~~~~~~

A simulator is given in the source: ``simu.py`` it is based on simgrid
(https://simgrid.frama.io/).

- Reserve a node interactively
- Play with the simulator (you can change the parameter at the end of the file

  - Create a virtualenv (isolated python installation): ``node) virtualenv -p python3 venv``

  - Jump into the virtualenv: ``node) source venv/bin/activate``

  - Install the requirements (recompile simgrid so don't do that on a frontend): ``node) pip install -v -r requirements.txt``

  - Configure your env (allows to get access to the shared library required by the python program): ``node) source setup.sh``

- Note that the results are appended in the ``result.json`` file

- Lab assignement:
    - play the simulation for many input parameters and get the result
      (uncomment the loop in the source file)
    - visualize the result in the jupyter notebook ``simu.ipynb``.
        - e.g run jupyter in a node of grid5000 or in your machine
        - This might be helpful: https://www.grid5000.fr/w/FAQ#How_can_I_connect_to_an_HTTP_or_HTTPS_service_running_on_a_node.3F
        or https://www.grid5000.fr/w/SSH#TCP_port_forwarding


Experimentation
~~~~~~~~~~~~~~~

We'll want to compare the model with a real setup.

- First, reserve two distinct nodes on Grid'5000, and get the characteristics
  of the network link between them. (Make one reservation with two nodes)
- Use `oarsh` to connect from one node to another: https://www.grid5000.fr/w/Getting_Started
- Use `oarsub -C <job_id>` if necessary to connect to your job
- Run ``iperf`` with different sizes between the two nodes
- Compare with the model

In order to control the link characteristics between the nodes we propose, in
the next section, to use some emulation techniques.


Emulation
~~~~~~~~~

You're in the wild now :).

As a starting point we give:

- ``emulation.py``: a script that starts two nodes and launch iperf between
  them.
  Note: It doesn't use the command line (e.g ``oarsub``) but
  rather some high level API (REST API). It has to be launched from the
  frontend of a site.
- It uses EnOSlib: https://discovery.gitlabpages.inria.fr/enoslib/
- Network emulation can be set using the following: https://discovery.gitlabpages.inria.fr/enoslib/apidoc/service.html#network-emulation-netem-simplenetem