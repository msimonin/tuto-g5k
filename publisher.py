#!/usr/bin/env python
import pika
import sys
import time


_, host, number, sleep = sys.argv

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=host))
channel = connection.channel()

channel.queue_declare(queue='task_queue')


for i in range(int(number)):
    message = f"[{i+1}/{number}] Hello World!"
    channel.basic_publish(
        exchange='',
        routing_key='task_queue',
        body=message,
        properties=pika.BasicProperties(
            delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE
        ))
    print(f" [x] Sent {message}")
    time.sleep(float(sleep))
    
connection.close()