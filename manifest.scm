;; This file defines the runtime environment for the workflow in Guix.
;; To run the workflow under Guix in a fully isolated environment, do
;;
;;    guix shell -C -N -m manifest.scm
;; 
;;    guix shell -C -N -m manifest.scm -- ipython
;;
;; in the directory containing this file.
;;
(specifications->manifest
 '(
   "coreutils"
   "python"
   "python-ipython"
   "jupyter"
   "python-matplotlib"
   "python-pandas"
   "python-plotly"
   "python-grid5000"
   "python-pytz"
   ))
